from turtle import *


def square(turtle, size):
    turtle.forward(size)
    turtle.right(90)
    turtle.forward(size)
    turtle.right(90)
    turtle.forward(size)
    turtle.right(90)
    turtle.forward(size)
    turtle.right(90)


space = Screen()
zOnny = Turtle()
square(zOnny, 100)
square(zOnny, 75)
square(zOnny, 50)
square(zOnny, 25)
input()
