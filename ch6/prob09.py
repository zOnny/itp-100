from turtle import *


def square(draw, size):
    draw.forward(size)
    draw.right(90)
    draw.forward(size)
    draw.right(90)
    draw.forward(size)
    draw.right(90)
    draw.forward(size)
    draw.right(90)


space = Screen()
zOnny = Turtle()
square(zOnny, 100)
square(zOnny, 75)
square(zOnny, 50)
square(zOnny, 25)
input()
