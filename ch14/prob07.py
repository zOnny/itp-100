from turtle import *      # use the turtle library
import random 

space = Screen()          # create a turtle screen (space)
width = space.window_width()
height = space.window_height()
max_x = width // 2  # get the max x value
min_x = -1 * max_x
max_y = height // 2
min_y = -1 * max_y
jaz = Turtle() # create a turtle named jaz

for num in range(10):
    if num % 2 == 0:             # if even row
        jaz.color('red')          # set the color to red
    else:
        jaz.color('black')       # set the color to black
    rand_x = random.randrange(min_x, max_x)
    rand_y = random.randrange(min_y, max_y)
    jaz.goto(rand_x,rand_y)

