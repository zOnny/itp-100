from turtle import *             # use the turtle library
from sys import *                # use the system library

space = Screen()                 # create a turtle screen (space)

width = space.window_width()     #get the width of the screen (space)
max_x = width // 2             #set the max x value to half the width

sue = Turtle()                   # create a turtle named sue
sue.pensize(10)                  # set the pen width
sue.left(90)

for y in range(5):               # repeat 5 times
    sue.penup()                      # pick up the pen
    if y % 2 == 0:                   # if even row
        sue.color('yellow')                 # set the color to yellow
    if y % 2 == 1:                   # if odd row
        sue.color('black')               # set the color to black
    sue.goto(y * 10, -1 * max_x,)     # move to the next row
    sue.pendown()                    # put the pen down
    sue.forward(width)               # move forward by the width

