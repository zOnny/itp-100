from turtle import *             # use the turtle library
from sys import *                # use the system library

space = Screen()                 # create a turtle screen (space)

height = space.window_height()   # get the height of the screen (space)
width = space.window_width()       # get the width
max_y = height // 2              # set the max y value to half the height
max_x = width

sue = Turtle()                   # create a turtle named sue
sue.pensize(10)                  # set the pen width

for y in range(4):               # repeat 5 times
    sue.penup()                      # pick up the pen
    if y % 2 == 0:                   # if even row
        sue.color('yellow')              # set the color to yellow
        sue.goto(-1 * max_x, y * 10)
        sue.pendown()
        sue.forward(width)
        sue.setheading(90)
    if y % 2 == 1:                   # if odd row
        sue.color('black')           # set the color to black
        sue.goto(y * 10, -1 * max_y)
        sue.pendown()
        sue.forward(height)
        sue.setheading(0)
