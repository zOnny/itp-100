from turtle import  *      # use the turtle library
from sys import *         # use the system library
space = Screen()

def turtle_loop(t, max_x):
    for x in range(10):        # repeat the body 10 times
        t.forward(100)            # go forward 100
        t.right(120)             # turn right 120 degrees
        t.forward(100)           # go forward 100
        t.left(120)               # turn left 120 degrees
        if (t.xcor() >= max_x):  # if at right edge of space
            t.penup()                # pick up the pen
            t.goto(-1 * max_x, jaz.ycor() - 100)  # move left & down
            t.pendown()              # put the pen down


def turtle_procedure(w, t):
    space.setup(w, w)         # set the space width and height
    max_x = w // 2            # set the max x value to half the width
    t.shape('turtle')         # set the shape for jaz to turtle
    t.penup()                 # pick up the pen (don't draw)
    t.goto(-1 * max_x, 100)   # go to the left side of the space
    t.pendown()               # put the pen down to draw with
    t.left(60)                # turn the turtle left 60 degrees
    turtle_loop(t, max_x)     # call the other function

width = 400                  # set the desired width
jaz = Turtle()               # create a turtle named jaz
turtle_procedure(width, jaz)

