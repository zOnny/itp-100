print("You are in front of a creepy door in a hallway.")
print("What do you want to do?")
user_input = input("Type: in, left, or right. Then click OK or press enter.")
if user_input == "in":
    print("You choose to go in.")
    print("The room is pitch black.")

if user_input == "left":
    print("You choose to turn left.")
    print("A ghost appears at the end of the hall.")
    
if user_input == "right":
    print("You choose to turn right.")
    print("A greenish light is visible in the distance.")
