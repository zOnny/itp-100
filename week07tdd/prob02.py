def num_even_digits(n):
    """
      >>> num_even_digits(123456)
      3
      >>> num_even_digits(2468)
      4
      >>> num_even_digits(1357)
      0
      >>> num_even_digits(2)
      1
      >>> num_even_digits(20)
      2
    """
    even_digits = 0
    while n > 0:
        a = n % 10
        if a % 2 == 0:
            even_digits += 1
        n //= 10

    return even_digits

            
