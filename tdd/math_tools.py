def hypotenuse(a, b):
    """
      >>> hypotenuse(3, 4)
      5.0
      >>> hypotenuse(12, 5)
      13.0
    """
    return (a ** 2 + b ** 2) ** 0.5


def slope(x1, y1, x2, y2):
    """
      >>> slope(5, 3, 4, 2)
      1.0
      >>> slope(1, 2, 3, 2)
      0.0
    """
    return (y2 - y1) / (x2 - x1)


def intercept(x1, y1, x2, y2):
    """
      >>> intercept(1, 6, 3, 12)
      3.0
      >>> intercept(6, 1, 1, 6)
      7.0
      >>> intercept(4, 6, 12, 8)
      5.0
    """
    m = (y2 - y1) / (x2 - x1)
    y = y1
    x = x1
    b = y - m * x
    return b


def is_even(n):
    """
      >>> is_even(2)
      True
      >>> is_even(1)
      False
    """
    return n % 2 == 0


def is_odd(n):
    """
      >>> is_odd(4)
      False
      >>> is_odd(3)
      True
    """
    return n % 2 == 1


def is_factor(f, n):
    """
      >>> is_factor(3, 12)
      True
      >>> is_factor(5, 12)
      False
      >>> is_factor(7, 14)
      True
      >>> is_factor(2, 14)
      True
      >>> is_factor(7, 15)
      False
    """
    return n % f == 0


def is_multiple(m, n):
    """
      >>> is_multiple(12, 3)
      True
      >>> is_multiple(12, 4)
      True
      >>> is_multiple(12, 5)
      False
      >>> is_multiple(12, 6)
      True
      >>> is_multiple(12, 7)
      False
    """
    return m % n == 0


if __name__ == '__main__':
    import doctest
    doctest.testmod()
