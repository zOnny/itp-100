def print_digits(n):
    """
      >>> print_digits(13789)
      9 8 7 3 1
      >>> print_digits(39874613)
      3 1 6 4 7 8 9 3
      >>> print_digits(213141)
      1 4 1 3 1 2
      >>> returned = print_digits(123)
      3 2 1
      >>> print(returned)
      None
    """
    while n > 0:
        print(n % 10, end=' ')
        n //= 10

    #strn = str(n)
    #for ch in range(len(strn)-1, -1, -1):
     #   print(strn[ch], end=' ')


